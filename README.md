# Supreme-Schmidt-Scripts but only neovim config
**_PEACE AND TRANQUILITY_**

![ezgif-6-22a5339a8fa2](https://user-images.githubusercontent.com/51456769/143289883-5bb4ea81-a51c-4ef0-8808-b2e46f0c782a.gif)


**init.nvim**: equivalente ao .vimrc

**backup_files**: alguns backups que eu mantenho para não precisar gravar código

**lua**: Alguns arquivos de configuração dos plugins que usam lua

**templates**: Arquivos iniciais de alguns tipos de arquivos (c,html,bash,etc)

**coc-settings.json**: Arquivo de configuração do plugin Conquer of completition (coc)

**ftdetect/ e syntax/**: Usados para o highlight da linguagem obscura .twine
