call plug#begin('~/.config/nvim/plugged')

Plug 'kaicataldo/material.vim', { 'branch': 'main' }

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'mg979/vim-visual-multi', {'branch': 'master'}
" Plug 'andweeb/presence.nvim'
Plug 'lambdalisue/suda.vim'

Plug 'nvim-lualine/lualine.nvim'

Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.0' }

Plug 'nvim-treesitter/nvim-treesitter'
Plug 'windwp/nvim-autopairs'

call plug#end()

lua require('nvim-tree-config')
lua require('lualine-config')
lua require('nvim-treesitter-config')
lua require("nvim-autopairs").setup {}
" lua require('nvim-autotag-config')


" abre esse arquivo usando o \rc
:nnoremap <leader>rc :vsplit $MYVIMRC<cr>

" para o vim:
"command SW :execute ':w !sudo tee > /dev/null %' | :edit!
" para o neovim:
command! SW SudaWrite

" evitar o shift quando detecta um erro
set signcolumn=yes
set showcmd " mostra a linha de comando
set showmatch " marca o parentesis par
set smartcase " ignora maiusculas apenas quando não há maiusculas
" set autowrite " 
set mouse=a " permite uso do mouse
" set nocompatible 
set backspace=2 " permite uso do backspace no insert mode
map <leader><space> :let @/=''<cr> 

" permite numeros hibridos
set number
set relativenumber

" alias :Wq = :wq
command! Wq wq
command! W w
command! WQ wq
command! Q q

" simplifica indexação
set tabstop=3
set shiftwidth=3
set softtabstop=3
set autoindent
set smartindent

" list chars
set list lcs=tab:┆\ 

set matchpairs+=<:>
set ttyfast

set laststatus=2
set encoding=utf-8

" theme
" syntax on
set termguicolors
let g:material_terminal_italics = 1
let g:material_theme_style = 'ocean'
colorscheme material

set hlsearch
highlight CocHintFloat ctermfg=Red  guifg=#ff0000

" Permite o uso de skeletons
" augroup templates
" 	au!
" 	autocmd BufNewFile *.* silent! execute '0r $HOME/.config/nvim/templates/skel.'.expand("<afile>:e")
" augroup END

" ------------------------------------------------------------------------------------------ "
" Funções para o set paste autoToggle
" ------------------------------------------------------------------------------------------ "

let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

" ------------------------------------------------------------------------------------------ "
" Funções para o COC (Conquer of completion)
" ------------------------------------------------------------------------------------------ "

" Lista as extensões
let g:coc_global_extensions = ['coc-clangd','coc-cmake','coc-docker','coc-java','coc-sql','coc-sh','coc-python','coc-tsserver','coc-json','coc-html','coc-snippets']

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

  
" Trigger completion with tab
  " remap for complete to use tab and <cr>
function! CheckBackspace() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction
inoremap <silent><expr> <TAB>
	  \ coc#pum#visible() ? coc#pum#next(1):
	  \ CheckBackspace() ? "\<Tab>" :
	  \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" ------------------------------------------------------------------------------------------ "
" Função para o close tag
" ------------------------------------------------------------------------------------------ "

let g:closetag_xhtml_filetypes = 'xhtml,jsx,js'
