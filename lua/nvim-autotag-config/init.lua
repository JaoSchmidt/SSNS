require('nvim-ts-autotag').setup({
	filetypes = { 'html' , 'xml', 'javascript','typescript','javascriptreact', 'typescriptreact', 'svelte', 'vue', 'tsx', 'jsx','php','markdown'}
})
